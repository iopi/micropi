# MICROPI
MicroPi ist ein Embedded Linux für den Raspberry Pi Zero (W). Das Ziel ist, den Pi so zu konfigurieren, dass er über USB mit der Arduino IDE (arduino.cc) programmiert werden kann. Um dies zu ermöglichen, müssen zusätzlich noch einige Anpassungen auf dem Host-Rechner gemacht werden, auf dem die IDE läuft. 

##	MICROPI INSTALLIEREN
Um MicroPi zu installieren benötigen Sie ein entsrprechendes SD-Karten-Image. Dieses können Sie entweder selbst erstellen (siehe unten) oder unter folgender Adresse herunterladen: https://gitlab.informatik.hu-berlin.de/iopi/micropi/tags

Die Installation von MicroPi erfolgt analog zu vielen anderen Raspberry-Pi-Systemen. Weitere Informationenn finden Sie z.B. unter https://www.raspberrypi.org/documentation/installation/installing-images/README.md Unter Linux kann die Installation mit folgendem Befehl erfolgen: 

	dd if=micropi.img of=/dev/sdX

wobei `micropi.img` das MicroPi-Image und `/dev/sdX` die SD-Karte ist, auf die das Image geschrieben werden soll.

Wenn Sie MicroPi nicht selbst kompiliert haben, können Sie nach dem Beschreiben der SD-Karte die WLAN-Konfiguration vornehmen. Dazu mounten Sie die SD-Karte und öffnen auf der Partition `rootfs` die Datei

	/etc/wpa_supplicant.conf

Passen Sie die Datei nach Ihren Bedürfnissen an. Weitere Information finden Sie in den Kommentaren der Datei selbst oder z.B. unter: https://www.systutorials.com/docs/linux/man/5-wpa_supplicant.conf/  

##	MICROPI KOMPILIEREN
Sie können MicroPi unter Linux mithilfe von [Buildroot](https://buildroot.uclibc.org/) selbst kompilieren. Installieren Sie zunächst die folgenden Pakete:

    apt-get install build-essential libncurses5-dev

Danach klonen Sie die Git-Repositories für Buildroot und MicroPi:

    git clone git://git.buildroot.net/buildroot
	git clone git@gitlab.informatik.hu-berlin.de:iopi/micropi.git

Wechseln Sie in das Buildroot-Verzeichnis und laden Sie die MicroPi-konfiguration:

    cd buildroot
	make BR2_EXTERNAL=/path/to/micropi/ micropi_defconfig

Bevor Sie die Kompilation nun starten, kann es sinnvoll sein, einige Anpassungen vorzunehmen, insbesondere die WLAN-Konfiguration. Öffnen Sie dazu die Datei

	micropi/rootfs-overlay/etc/wpa_supplicant.conf

und passen Sie sie nach Ihren Bedürfnissen an. Weitere Information finden Sie in den Kommentaren der Datei selbst oder z.B. unter: https://www.systutorials.com/docs/linux/man/5-wpa_supplicant.conf/  

Um MicroPi zu kompilieren, führen Sie `make` im Verzeichnis `buildroot/` aus. Der Prozess kann einige Zeit dauern, vor allem wenn beim ersten Mal entsprechende Quellen zuerst herunter geladen werden müsse. Wenn der Prozess erfolgreich abgeschlossen wurde, finden sie das MicroPi-Image unter `buildroot/output/images/sdcard.img`

## HOST-RECHNER KONFIGURIEREN

### USB-TREIBER
Verbinden sie den Datenanschluss des Raspberry Pi (mittlerer USB-Port) mit einem Host-Rechner. Der Pi wird versuchen, eine virtuele Netzwerkverbindung zu erzeugen. Hierzu müssen jedoch ggf. noch entsprechende Treiber auf dem Host installiert werden.
- **Linux:** In der Regel keine manuelle Konfiguration nötig.
- **Windows 7:** Wenn die automatische Treiber-Installation fehl schlägt, Treiber wie folgt manuell installieren: http://developer.toradex.com/knowledge-base/how-to-install-microsoft-rndis-driver-for-windows-7
- **Windows 8:** ???
- **Windows 10:** Wenn weder die automatische noch die manuelle Treiber-Installation (s. Windows 7) zum Ziel führen, kann folgendender Treiber verwendet werden: https://wiki.moddevices.com/wiki/Troubleshooting#Windows_10_.2832-bit.29

Nach erfolgreicher Treiber-Installation (und evtl. Neu-Verbinden des Pi), sollte der Host-Rechner über eine neue Netzerk-Schnittstelle mit einer IP der Form 10.0.0.x verfügen. Der Pi sollte unter der IP 10.0.0.1 erreichbar sein, z.B. via Ping oder SSH (s. unten).

### ARDUINO IDE
Installieren sie die [Arduino IDE](https://www.arduino.cc/en/Main/Software) und folgen Sie dem Abschnitt "Instructions for Arduino IDE" auf der folgenden Webseite: https://github.com/me-no-dev/RasPiArduino Der Abschnitt "Instructions for the Pi" kann ignoriert werden. Dieser Teil ist in MicroPi vorkonfiguriert.

Wenn alles funktioniert hat, wählen sie in der Arduino IDE unter `Werkzeuge > Board` den `Raspberry Pi B+2` aus. Wenn sie den Pi erfolgreich über USB mit dem Rechner verbunden haben (s. USB-Treiber), sollte er in der IDE unter `Werkzeuge > Port` mit der IP 10.0.0.1 aufgeführt werden.

## SSH
Sobald das virtuelle Netzwerk-Interface mit dem Raspberry Pi hergestellt ist (s. USB-Treiber), kann mit einem SSH-Client (unter Windows z.B. PuTTY) eine Verbindung aufgebaut werden. Melden Sie sich dazu einfach als `root@10.0.0.1` an. Eine Authentifikation ist nicht notwendig. Hier haben Sie Zugriff auf die Kommnadozeile von MicroPi und können das System nach Ihren Wünschen weiter konfigurieren. Insbesondere lassen sich auf diese Weise Netzwerk- (/etc/network/interfaces) und WLAN-Einstellungn (/etc/wpa_supplicant.conf) anpassen.