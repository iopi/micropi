#!/bin/sh

# Start a root shell on tty1 without asking for credentials
if [ -e $1/etc/inittab ]; then
    echo "Modifying shell startup in $1/etc/inittab\n";
    grep -qE '^tty1::' $1/etc/inittab || \
	sed -i '/GENERIC_SERIAL/a\
tty1::respawn:/bin/sh # auto root shell' $1/etc/inittab
fi


# Modify environment variables
if [ -e $1/etc/profile ]; then
   # Add /usr/local/bin to PATH
   echo "Modifying PATH variable in $1/etc/profile\n";
   sed -i '/^export PATH=/ s/$/:\/usr\/local\/bin/' $1/etc/profile;

   # Set http_proxy variable (comment out to disable http proxy)
   # echo "Setting http_proxy";
   # echo "export http_proxy=\"10.16.1.1:8080\"" >> $1/etc/profile;
else
   echo "$1/etc/profile: file not found\nWARNING: environment variables may not be set correctly!";
fi

# Add dtoverlays to config.txt
echo "Adding dtoverlays to config.txt:";
echo "dwc2: for USB OTG";
echo "i2c-bcm2708: for I2C devices (e.g., ADCs)";
if ! grep -qE '^dtoverlay=' "${BINARIES_DIR}/rpi-firmware/config.txt"; then
	cat << __EOF__ >> "${BINARIES_DIR}/rpi-firmware/config.txt"

# Needed for OTG mode
dtoverlay=dwc2

# Needed for I2C
dtoverlay=i2c-bcm2708
dtparam=i2c_arm=on
__EOF__
else
	sed -i "/^dtoverlay=/ s/$/,dwc2,i2c-bcm2708/" "${BINARIES_DIR}/rpi-firmware/config.txt";
fi

# Modify /etc/avahi/avahi-daemin.conf
echo "Configuring avahi daemon to only use USB interface"
sed -i '/allow-interfaces/c\allow-interfaces=usb0' $1/etc/avahi/avahi-daemon.conf
sed -i '/deny-interfaces/c\deny-interfaces=wlan0' $1/etc/avahi/avahi-daemon.conf

# Create symbolic links
echo "Creating symlinks:"
ln -vs /usr/local/bin/run-avrdude $1/usr/bin/run-avrdude
ln -vs /usr/local/bin/merge-sketch-with-bootloader.lua $1/usr/bin/merge-sketch-with-bootloader.lua
